using System;
using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Grill.Tests.EditMode
{
    public class GrillableDoubleSided_Should
    {
        [Test]
        public void Be_Uncooked_On_Creation()
        {
            var gameObject = new GameObject();
            var grillable = new GrillableDoubleSided(0f, 0f, 30f, gameObject.transform);
            Assert.AreEqual(0f, grillable.GradeCooked);
        }


        [Test]
        [TestCase(30f)]
        public void Be_Cooked_On_Bottom_After_Heat_Applied(float duration)
        {
            var gameObject = new GameObject();
            var transform = gameObject.transform;
            var grillable = new GrillableDoubleSided(0f, 0f, 30f, transform);

            grillable.ApplyHeat(-transform.up, duration);

            Assert.Greater(grillable.GradeCookedBottom, 0f);
        }
    }
}