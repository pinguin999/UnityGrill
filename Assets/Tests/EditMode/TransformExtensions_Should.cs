﻿using NUnit.Framework;
using UnityEngine;

namespace Grill.Tests.EditMode
{
    public class TransformExtensions_Should
    {
        [Test]
        public void Return_Direction_From_Bottom()
        {
            var zero = new GameObject().transform;
            var direction = zero.GetDirectionFrom(Vector3.down);
            Assert.AreEqual(-1, direction);
        }

        [Test]
        public void Return_Direction_From_Bottom_Right()
        {
            var zero = new GameObject().transform;
            var direction = zero.GetDirectionFrom(Vector3.down + Vector3.right);
            Assert.AreEqual(-1, direction);
        }

        [Test]
        public void Return_Direction_From_Top()
        {
            var zero = new GameObject().transform;
            var direction = zero.GetDirectionFrom(Vector3.up);
            Assert.AreEqual(1, direction);
        }

        [Test]
        public void Return_Direction_From_Top_Right()
        {
            var zero = new GameObject().transform;
            var direction = zero.GetDirectionFrom(Vector3.up + Vector3.right);
            Assert.AreEqual(1, direction);
        }

        [Test]
        public void Return_Direction_From_One()
        {
            var zero = new GameObject().transform;
            var direction = zero.GetDirectionFrom(Vector3.one);
            Assert.AreEqual(1, direction);
        }

        [Test]
        public void Return_Direction_From_Zero()
        {
            var zero = new GameObject().transform;
            var direction = zero.GetDirectionFrom(Vector3.zero);
            Assert.AreEqual(0, direction);
        }

        [Test]
        public void Return_Direction_From_Left()
        {
            var zero = new GameObject().transform;
            var direction = zero.GetDirectionFrom(Vector3.left);
            Assert.AreEqual(0, direction);
        }

        [Test]
        public void Return_Direction_From_Right()
        {
            var zero = new GameObject().transform;
            var direction = zero.GetDirectionFrom(Vector3.right);
            Assert.AreEqual(0, direction);
        }
    }
}