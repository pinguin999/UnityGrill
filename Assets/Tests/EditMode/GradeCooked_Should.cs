using System;
using System.Collections;
using NUnit.Framework;
using UnityEngine.TestTools;
using static Grill.GrillFormulas;

namespace Grill.Tests.EditMode
{
    public class GrillFormulas_Should
    {
        [Test]
        [TestCase(0.5f, 0.5f, ExpectedResult = 0.5f)]
        [TestCase(0f, 0.5f, ExpectedResult = 0.25f)]
        [TestCase(0f, 1f, ExpectedResult = 0.5f)]
        [TestCase(1f, 1f, ExpectedResult = 1f)]
        [TestCase(1.5f, 1f, ExpectedResult = 0.75f)]
        [TestCase(1.5f, 1.5f, ExpectedResult = 0.5f)]
        [TestCase(2f, 2f, ExpectedResult = 0f)]
        [TestCase(20f, 20f, ExpectedResult = -18f)]
        public float Return_Combined_Cooked_Grade(float top, float bottom)
        {
            var overall = CombinedGradeCooked(top, bottom);
            return overall;
        }

        [Test]
        [TestCase(30f, 30f, ExpectedResult = 1f)]
        [TestCase(15f, 30f, ExpectedResult = 0.5f)]
        [TestCase(60f, 30f, ExpectedResult = 0f)]
        public float Return_Cooked_Amount(float duration, float timeToCook)
            => GetCookedAmount(duration, timeToCook);
    }
}