using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Grill
{
    public class FollowPointerBehaviour : MonoBehaviour
    {
        [SerializeField] private float _distance = 50f;
        [SerializeField] private bool _look_and_offset = false;

        private Camera _camera;

        private void Awake()
        {
            _camera = Camera.main;
        }

        private void Update()
        {
            var ray = _camera.ScreenPointToRay(Input.mousePosition);

            if (!Physics.Raycast(ray, out var hit, _distance)) return;

            if (_look_and_offset)
            {
                transform.LookAt(hit.point);
                var pos = transform.position;
                pos.z = hit.point.z - .46f;
                pos.x = hit.point.x + .3f;
                transform.position = pos;
            }
            else
            {
                var pos = transform.position;
                pos.z = hit.point.z;
                pos.x = hit.point.x;
                // pos = hit.point;
                transform.position = pos;
            }

            Debug.DrawLine(ray.origin, hit.point);
        }
    }
}