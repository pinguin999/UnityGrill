using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Grill
{
    public class SpawnBehaviour : MonoBehaviour
    {
        [SerializeField] private GameObject _prefab;

        [SerializeField] private Transform _spawnPoint;


        [ContextMenu("Spawn")]
        public void Spawn()
        {
            Instantiate(_prefab, _spawnPoint.position, Quaternion.identity, transform);
        }
    }
}