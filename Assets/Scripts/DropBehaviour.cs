using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Grill
{
    public class DropBehaviour : MonoBehaviour, IDropHandler
    {
        public UnityEventGameObject Dropped;

        public void OnDrop(PointerEventData eventData)
        {
            var other = eventData.pointerDrag;
            Dropped?.Invoke(other);
        }
    }
}