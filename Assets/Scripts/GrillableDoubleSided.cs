﻿using UnityEngine;
using static Grill.GrillFormulas;

namespace Grill
{
    public class GrillableDoubleSided : IGrillable
    {
        public float GradeCookedTop { get; private set; }
        public float GradeCookedBottom { get; private set; }
        public float GradeCooked => CombinedGradeCooked(GradeCookedTop, GradeCookedBottom);
        private Transform _transform;

        public float TimeToCook { get; private set; } = 30f;

        public GrillableDoubleSided(float top, float bottom, float timeToCook, Transform transform)
        {
            GradeCookedTop = top;
            GradeCookedBottom = bottom;
            TimeToCook = timeToCook;
            _transform = transform;
        }

        public void ApplyHeat(Vector3 origin, float duration)
        {
            var direction = _transform.GetDirectionFrom(origin);
            if (direction == 0) return;

            var amount = GetCookedAmount(duration, TimeToCook);

            if (direction > 0)
                GradeCookedTop += amount;
            else
                GradeCookedBottom += amount;
        }
    }
}