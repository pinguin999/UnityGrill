using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Grill
{
    public class FoodPlaceBehaviour : MonoBehaviour
    {
        public void FoodPlaced(GameObject foodObject)
        {
            var plateable = foodObject.GetComponent<IPlateable>();
            if (plateable == null) return;

            plateable.ToPlate();
        }
    }
}