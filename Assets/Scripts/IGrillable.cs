﻿using UnityEngine;

namespace Grill
{
    public interface IGrillable
    {
        float GradeCooked { get; }

        void ApplyHeat(Vector3 origin, float duration);
    }
}