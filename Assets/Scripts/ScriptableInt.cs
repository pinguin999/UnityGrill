using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Grill
{
    [CreateAssetMenu(menuName = "Grillmeister/ScriptableInt", fileName = "New ScriptableInt")]
    public class ScriptableInt : ScriptableObject
    {
        [SerializeField] private int _value;
        public int Value => _value;

        public event Action<int> OnUpdate;

        private void OnEnable()
        {
            Reset();
        }

        public void Reset()
        {
            _value = 0;
            OnUpdate?.Invoke(_value);
        }

        public void Add(int amount)
        {
            _value += amount;
            OnUpdate?.Invoke(_value);
        }
    }
}