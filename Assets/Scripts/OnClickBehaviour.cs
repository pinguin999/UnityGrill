using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Grill
{
    public class OnClickBehaviour : MonoBehaviour, IPointerClickHandler
    {
        public UnityEvent OnClick;

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick?.Invoke();
        }


    }
}