using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Grill
{
    public static class GrillFormulas
    {
        public static float CombinedGradeCooked(float top, float bottom)
        {
            if (top > 1f) top = 1 - (top - 1);
            if (bottom > 1f) bottom = 1 - (bottom - 1);
            var overall = (top + bottom) / 2f;

            return overall;
        }

        public static float GetCookedAmount(float duration, float timeToCook)
        {
            var percentage = duration / timeToCook;
            if (percentage > 1f) percentage = 1 - (percentage - 1);
            return percentage;
        }
    }
}