using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Grill
{
    public class PlateableBehaviour : MonoBehaviour, IPlateable
    {
        [SerializeField] private int _points = 100;

        [SerializeField] private ScriptableInt _score;

        public void ToPlate()
        {
            _score.Add(_points);

            Destroy(gameObject);
        }
    }
}