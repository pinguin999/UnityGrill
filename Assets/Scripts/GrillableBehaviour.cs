using UnityEngine;
using static Grill.GrillFormulas;

namespace Grill
{
    public class GrillableBehaviour : MonoBehaviour, IGrillable
    {
        [SerializeField] private float _timeToCook = 30f;

        public float GradeCooked => CombinedGradeCooked(_gradeCookedTop, _gradeCookedBottom);

        private float _gradeCookedTop;
        private float _gradeCookedBottom;

        public void ApplyHeat(Vector3 origin, float duration)
        {
        }
    }
}