﻿namespace Grill
{
    public interface IPlateable
    {
        void ToPlate();
    }
}