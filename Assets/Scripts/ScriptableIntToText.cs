using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Grill
{
    [RequireComponent(typeof(TMP_Text))]
    public class ScriptableIntToText : MonoBehaviour
    {
        [SerializeField] private ScriptableInt _scriptableInt;

        private TMP_Text _tmpText;

        private void Awake()
        {
            _tmpText = GetComponent<TMP_Text>();
        }

        private void OnEnable()
        {
            _scriptableInt.OnUpdate += HandleValueUpdate;
            HandleValueUpdate(_scriptableInt.Value);
        }

        private void HandleValueUpdate(int value)
        {
            _tmpText.text = value.ToString();
        }

        private void OnDisable()
        {
            _scriptableInt.OnUpdate -= HandleValueUpdate;
        }
    }
}