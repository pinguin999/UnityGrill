using UnityEngine;

namespace Grill
{
    public static class TransformExtensions
    {
        /// <summary>
        /// Get a direction the origin points from
        /// </summary>
        /// <param name="vector3">stationary point</param>
        /// <param name="origin">coming from point</param>
        /// <returns>1: closer to top face
        /// -1: closer to bottom face
        /// 0: no direction could be detected</returns>
        public static int GetDirectionFrom(this Transform transform, Vector3 origin)
        {
            var dotUp = Vector3.Dot(transform.up, origin);
            if (dotUp > 0f)
                return 1;
            if (dotUp < 0f)
                return -1;
            return 0;
        }
    }
}