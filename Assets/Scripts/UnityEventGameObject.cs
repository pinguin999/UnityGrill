﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Grill
{
    [Serializable]
    public class UnityEventGameObject : UnityEvent<GameObject>
    {
    }
}