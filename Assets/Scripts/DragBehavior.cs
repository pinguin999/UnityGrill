using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace Grill
{
    public class DragBehavior : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        public UnityEvent beginDrag;
        public UnityEvent endDrag;

        public void OnBeginDrag(PointerEventData eventData)
        {
            beginDrag?.Invoke();
        }

        public void OnDrag(PointerEventData eventData)
        {
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            endDrag?.Invoke();
        }
    }
}