using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Grill
{
    [RequireComponent(typeof(Animator))]
    public class GrabAnimationBehaviour : MonoBehaviour
    {
        private Animator _animator;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        public void Update()
        {
            _animator.SetBool("grab", Input.GetMouseButton(0));
        }
    }
}
